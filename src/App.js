import './App.css';
import { Provider } from 'react-redux';
import { NavBar } from './components';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Home, UserDetails } from './pages';
import { store } from './redux/store';

function App() {
  return (
    <Provider store={store}>
      <NavBar />
      <Router>
        <Routes>
          <Route exact path="*" element={<Home />}></Route>
          <Route exact path="/user/:id" element={<UserDetails />}></Route>
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
