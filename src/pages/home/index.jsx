import React, { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllUsersThunk } from '../../redux/thunks/userThunk';
import { selectUsers } from '../../redux/slices/userSlice';
import { MenuItem, Select, TextField } from '@mui/material';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: 'white'
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0
  }
}));

const Home = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { profiles } = useSelector(selectUsers);
  const [search, setSearch] = useState();
  const [filter, setFilter] = useState(' ');

  useEffect(() => {
    dispatch(getAllUsersThunk('?' + search + filter));
  }, [dispatch, search, filter]);

  function handleClick(id) {
    navigate('/user/' + id);
  }

  const onSearch = (e) => {
    setSearch('home_town=' + e.target.value);
  };

  const filterChange = (e) => {
    setFilter(e.target.value);
  };

  return (
    <Card
      sx={{
        paddingLeft: '70px',
        paddingRight: '70px',
        paddingTop: '30px',
        paddingBottom: '100%',
        marginBottom: '10px',
        marginTop: '10px'
      }}>
      <div style={{ marginBottom: 20 }}>
        <TextField
          id="outlined-basic"
          label="recherche par adresse"
          variant="outlined"
          onChange={onSearch}
        />
        <Select
          sx={{ marginLeft: 10, width: 500 }}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Filter personalisé"
          value={filter}
          onChange={filterChange}>
          <MenuItem value={' '}>Tous les utilisateurs</MenuItem>
          <MenuItem value={'&age_greater=18&age_less=25'}>
            list des utilisateurs dont l’âge est compris entre 18 et 25 ans
          </MenuItem>
          <MenuItem value={'&age_greater=30'}>list des femmes âgées de 30 ans</MenuItem>
        </Select>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Nom</StyledTableCell>
              <StyledTableCell align="right">Prenom</StyledTableCell>
              <StyledTableCell align="right">gender</StyledTableCell>
              <StyledTableCell align="right">age</StyledTableCell>
              <StyledTableCell align="right">Adresse</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {profiles.map((row) => (
              <StyledTableRow key={row.id} selected={true} onClick={() => handleClick(row.id)}>
                <StyledTableCell component="th" scope="row">
                  {row.user.first_name}
                </StyledTableCell>
                <StyledTableCell align="right">{row.user.last_name}</StyledTableCell>
                <StyledTableCell align="right">{row.gender}</StyledTableCell>
                <StyledTableCell align="right">{row.age}</StyledTableCell>
                <StyledTableCell align="right">{row.home_town}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  );
};

export default Home;
