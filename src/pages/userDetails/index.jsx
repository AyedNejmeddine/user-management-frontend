import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Divider } from '@mui/material';
import { getAllUserByIdService } from '../../services';
import { Grid } from '@mui/material';

const UserDetails = () => {
  const { id } = useParams();
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    getUser();
  }, []);

  async function getUser() {
    setProfile(await getAllUserByIdService(id));
  }

  function getProfileItems() {
    if (profile !== undefined) {
      return (
        <div>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              Nom :
            </Grid>
            <Grid item xs={8}>
              {profile?.user.first_name}
            </Grid>
            <Grid item xs={4}>
              Prenom :
            </Grid>
            <Grid item xs={8}>
              {profile?.user.last_name}
            </Grid>
            <Grid item xs={4}>
              Email :
            </Grid>
            <Grid item xs={8}>
              {profile?.user.email}
            </Grid>
            <Grid item xs={4}>
              Gender :
            </Grid>
            <Grid item xs={8}>
              {profile?.gender}
            </Grid>
            <Grid item xs={4}>
              Age :
            </Grid>
            <Grid item xs={8}>
              {profile?.age}
            </Grid>
            <Grid item xs={4}>
              Adresse :
            </Grid>
            <Grid item xs={8}>
              {profile?.home_town}
            </Grid>
            <Grid item xs={4}>
              Date de rejoindre :
            </Grid>
            <Grid item xs={8}>
              {profile?.user.date_joined}
            </Grid>
          </Grid>
        </div>
      );
    }
    return <div>Loading ...</div>;
  }

  return (
    <Card sx={{ margin: 'auto', minHeight: 500, maxWidth: 500, marginTop: 2, padding: 5 }}>
      <CardContent>
        <div>
          <h3 style={{ textAlign: 'center' }}>Les details de l ulilisateur</h3>
          <Divider sx={{ marginBottom: 5 }} />
          {getProfileItems()}
        </div>
      </CardContent>
    </Card>
  );
};

export default UserDetails;
