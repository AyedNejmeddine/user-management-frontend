import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  profiles: []
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUsers: (state, action) => {
      state.profiles = action.payload;
    }
  }
});

export const selectUsers = (state) => state.user;
export default userSlice.reducer;
export const { setUsers } = userSlice.actions;
