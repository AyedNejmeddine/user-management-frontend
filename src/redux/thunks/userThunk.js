import { getAllUsersService } from '../../services';
import { setUsers } from '../slices/userSlice';

export const getAllUsersThunk = (params) => async (dispatch) => {
  try {
    const res = await getAllUsersService(params);
    dispatch(setUsers(res));
    return res;
  } catch {}
  return null;
};
