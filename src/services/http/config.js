const APP_API = process.env.APP_API ? process.env?.APP_API : 'http://localhost:8000/';
export const HttpConfig = {
  baseURL: APP_API
};
