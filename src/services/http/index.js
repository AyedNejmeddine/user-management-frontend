import axios from 'axios';
import { HttpConfig } from './config';

const Http = axios.create({ baseURL: HttpConfig.baseURL });

export default Http;
