import Http from '../http';

async function getAllUsersService(params) {
  const url = 'api/v1/profiles/' + params;
  const res = await Http.get(url);
  return res.data;
}

async function getAllUserByIdService(id) {
  const url = 'api/v1/profiles/' + id + '/';
  const res = await Http.get(url);
  return res.data;
}

export { getAllUsersService, getAllUserByIdService };
